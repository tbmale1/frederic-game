import os
import pygame
import config
from game_state import GameState
import pygame.locals


#pygame.init()
#sound = pygame.mixer.Sound('sound_remix.wav')
#sound.play(loops=5)

#def menu():
#    global p_rect
#    global e_rect
#    if p_rect.colliderect(e_rect):
#        execfile('menu.py')

screen = pygame.display.set_mode((config.SCREEN_WIDTH, config.SCREEN_HEIGHT))


pygame.display.set_caption("Pokemon : La revanche des ISNiens")

clock = pygame.time.Clock()

surfaceW = config.SCREEN_WIDTH #Dimension de la fenêtre / Largeur
surfaceH = config.SCREEN_HEIGHT #Dimension de la fenêtre / Longueur


class Menu :
    """ Création et gestion des boutons d'un menu """
    def __init__(self, application, *groupes) :
        self.couleurs = dict(
            normal=(0, 200, 0),
            survol=(0, 200, 200),
        )
        self.bg = pygame.transform.scale(pygame.image.load(os.path.join('menubackground.png')).convert(),[surfaceW,surfaceH])
        font = pygame.font.SysFont('Helvetica', 24, bold=True)
        pygame.mixer.music.load('sound_remix.wav')
        pygame.mixer.music.set_volume(.3)
        pygame.mixer.music.play()
        # noms des menus et commandes associées
        items = (
            ('JOUER', application.jeu),
            ('QUITTER', application.quitter)
        )
        x = 400
        y = 200
        self._boutons = []
        for texte, cmd in items :
            mb = MenuBouton(
                texte,
                self.couleurs['normal'],
                font,
                x,
                y,
                200,
                50,
                cmd
            )
            self._boutons.append(mb)
            y += 120
            for groupe in groupes :
                groupe.add(mb)

    def update(self, events) :
        clicGauche, *_ = pygame.mouse.get_pressed()
        posPointeur = pygame.mouse.get_pos()
        app.fenetre.fill([255, 255, 255])
        app.fenetre.blit(self.bg, [0,0])
        for bouton in self._boutons :
            # Si le pointeur souris est au-dessus d'un bouton
            if bouton.rect.collidepoint(*posPointeur) :
                # Changement du curseur par un quelconque
                pygame.mouse.set_cursor(*pygame.cursors.tri_left)
                # Changement de la couleur du bouton
                bouton.dessiner(self.couleurs['survol'])
                # Si le clic gauche a été pressé
                if clicGauche :
                    # Appel de la fonction du bouton
                    bouton.executerCommande()
                break
            else :
                # Le pointeur n'est pas au-dessus du bouton
                bouton.dessiner(self.couleurs['normal'])
        else :
            # Le pointeur n'est pas au-dessus d'un des boutons
            # initialisation au pointeur par défaut
            pygame.mouse.set_cursor(*pygame.cursors.arrow)

    def detruire(self) :
        pygame.mouse.set_cursor(*pygame.cursors.arrow) # initialisation du pointeur



class MenuBouton(pygame.sprite.Sprite) :
    """ Création d'un simple bouton rectangulaire """
    def __init__(self, texte, couleur, font, x, y, largeur, hauteur, commande) :
        super().__init__()
        self._commande = commande

        self.image = pygame.Surface((largeur, hauteur))

        self.rect = self.image.get_rect()
        self.rect.center = (x, y)

        self.texte = font.render(texte, True, (0, 0, 0))
        self.rectTexte = self.texte.get_rect()
        self.rectTexte.center = (largeur/2, hauteur/2)

        self.dessiner(couleur)

    def dessiner(self, couleur) :
        self.image.fill(couleur)
        self.image.blit(self.texte, self.rectTexte)

    def executerCommande(self) :
        # Appel de la commande du bouton
        self._commande()


class Jeu :
    """ Simulacre de l'interface du jeu """
    def __init__(self, jeu, *groupes) :
        self._fenetre = jeu.fenetre
        jeu.fond = (0, 0, 0)

        from itertools import cycle
        couleurs = [(0, 48, i) for i in range(0, 256, 15)]
        couleurs.extend(sorted(couleurs[1:-1], reverse=True))
        self._couleurTexte = cycle(couleurs)

        self._font = pygame.font.SysFont('Verdana', 36, bold=True)
        self.texte=self._font.render('Pokemon : La Revanche des ISNiens', True, [0,0,0])
        self.rectTexte = self.texte.get_rect()
        self.rectTexte.center = (surfaceW/2, surfaceH/2)
        self.montrerTexte = True
        # Création d'un event
        self._CLIGNOTER = pygame.USEREVENT + 1
        self.blink_count = 0 #combien de fois clignoter
        pygame.time.set_timer(self._CLIGNOTER, 80)
        self.start()

    def start(self) :
        from game import Game
        self.game = Game(screen)
        self.game.set_up()
        pygame.mixer.fadeout(1000)
        pygame.mixer.music.load('poke_road_sound.wav')
        pygame.mixer.music.set_volume(.3)
        pygame.mixer.music.play()


    def update(self, events) :
        for event in events :
            if event.type == self._CLIGNOTER :
                self.montrerTexte = (self.montrerTexte == False)
                self.blink_count += 1
                if self.blink_count == 30:
                    pygame.time.set_timer(self._CLIGNOTER, 0)
                    self.montrerTexte = False
                break
        if self.game.game_state == GameState.RUNNING:
            clock.tick(60)
            self.game.update(events)
            if self.montrerTexte:
                self._fenetre.blit(self.texte, self.rectTexte)
            pygame.display.flip()
        else:
            app.menu()


    def detruire(self) :
        pygame.time.set_timer(self._CLIGNOTER, 0) # désactivation du timer


class Application :
    """ Classe maîtresse gérant les différentes interfaces du jeu """
    def __init__(self) :
        pygame.init()
        pygame.display.set_caption("")
        pygame.mixer.init()

        self.fond = (150,)*3

        self.fenetre = pygame.display.set_mode((surfaceW,surfaceH))
        # Groupe de sprites utilisé pour l'affichage
        self.groupeGlobal = pygame.sprite.Group()
        self.statut = True

    def _initialiser(self) :
        try:
            self.ecran.detruire()
            # Suppression de tous les sprites du groupe
            self.groupeGlobal.empty()
        except AttributeError:
            pass

    def menu(self) :
        # Affichage du menu
        self._initialiser()
        self.ecran = Menu(self, self.groupeGlobal)


    def jeu(self) :
        # Affichage du jeu
        #sound.stop()
        self._initialiser()
        self.ecran = Jeu(self, self.groupeGlobal)


    #background = pygame.image.load('menubackground.png')

    def quitter(self) :
        self.statut = False

    def update(self) :
        events = pygame.event.get()

        for event in events :
            if event.type == pygame.QUIT :
                self.quitter()
                return

        self.fenetre.fill(self.fond)
        self.ecran.update(events)
        self.groupeGlobal.update()
        self.groupeGlobal.draw(self.fenetre)
        pygame.display.update()


app = Application()
app.menu()

clock = pygame.time.Clock()

while app.statut :
    app.update()
    clock.tick(30)

pygame.quit()
