""" map loading module """
import pygame
import pygame.locals

def load_tile_table(filename, width, height):
    """ split map image in multiple tiles array
        width = tile width
        height = tile height
        return tiles array
    """
    image = pygame.image.load(filename).convert()
    image_width, image_height = image.get_size()
    tile_table = []
    # we use integer division ( // ) because ranges integer parameters
    # if image width or height is not an exact multiple of tile width or heigth
    # the map will miss the rest of the division in pixels
    for tile_y in range(0, image_height//height):
        line = []
        tile_table.append(line)
        # for tile_y in range(0, image_height/height):
        for tile_x in range(0, image_width//width):
            rect = (tile_x*width, tile_y*height, width, height)
            line.append(image.subsurface(rect))
    return tile_table

if __name__ == '__main__':
    pygame.init()
    screen = pygame.display.set_mode([640,480])
    screen.fill((255, 255, 255))
    # table = load_tile_table("Map.png", 640, 960)
    # transform tile_size to variable
    tile_w = 16
    tile_h = 16
    table = load_tile_table("Map.png", tile_w, tile_h)
    for x, row in enumerate(table):
        for y, tile in enumerate(row):
            screen.blit(tile, (x*tile_w, y*tile_h))
    pygame.display.flip()
    while pygame.event.wait().type != pygame.locals.QUIT:
        pass
