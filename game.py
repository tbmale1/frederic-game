import pygame
import config
import math
from player import Player
from game_state import GameState
import map
import os
import sys
print(os.getcwd())

class Game:
    def __init__(self, screen):
        self.screen = screen
        self.objects = []
        self.game_state = GameState.NONE
        # map ilogical content information
        self.map = []
        # map graphical content information
        self.map_tiles = []
        self.camera = [0, 0]

    def set_up(self):
        self.player =  Player(33,45)
        self.objects.append(self.player)
        self.walk = pygame.mixer.Sound('Walking On Gravel-SoundBible.com-2023303198.wav')
        self.game_state = GameState.RUNNING

        self.load_map("map01")

    def update(self,events):
        self.screen.fill(config.BLACK)
        self.handle_events(events)

        self.render_map(self.screen)

        for object in self.objects:
            object.render(self.screen, self.camera)

    def handle_events(self,events):
        # for event in pygame.event.get():
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            #     handle key events
            elif event.type == pygame.KEYDOWN:
                self.walk.play()
                if event.key == pygame.K_ESCAPE:
                    self.game_state = GameState.ENDED
                elif event.key == pygame.K_z or event.key == pygame.K_UP: # up
                    self.move_unit(self.player, [0,-1])
                elif event.key == pygame.K_s or event.key == pygame.K_DOWN: # down
                    self.move_unit(self.player, [0,1])
                elif event.key == pygame.K_q or event.key == pygame.K_LEFT: # left
                    self.move_unit(self.player, [-1,0])
                elif event.key == pygame.K_d or event.key == pygame.K_RIGHT: # right
                    self.move_unit(self.player, [1,0])
            elif event.type == pygame.KEYUP:
                self.walk.stop()

    pygame.key.set_repeat(20,150)

    # loading image as a text file is incorrect
    #  def load_map(self, file_name):
    #     with open(file_name + ".png") as map_file:
    #         for line in map_file:
    #             tiles = []
    #             for i in range(0, len(line) - 1, 2):
    #                 tiles.append(line[i])
    #             self.map.append(tiles)
    #         print(self.map)

    def load_map(self, file_name):
        self.map_tiles = map.load_tile_table(file_name+".png", config.TILE_WIDTH, config.TILE_HEIGHT)

    def render_map(self, screen):
        self.determine_camera()

        y_pos = 0
        for line in self.map_tiles:
            x_pos = 0
            for tile in line:
                image = tile
                rect = pygame.Rect(
                 x_pos * config.SCALE * config.TILE_WIDTH - (self.camera[0] * config.SCALE * config.SCREEN_WIDTH),
                 y_pos * config.SCALE * config.TILE_HEIGHT - (self.camera[1] * config.SCALE * config.TILE_HEIGHT),
                 config.SCALE,
                 config.SCALE)
                screen.blit(image, rect)
                x_pos = x_pos + 1
            y_pos = y_pos + 1

    def move_unit(self, unit, position_change):
        new_position = [unit.position[0] + position_change[0] , unit.position[1] + position_change[1]]

        if new_position[0] < 0 or new_position[0] > (len(self.map_tiles[0]) - 1):
            return

        if new_position[1] < 0 or new_position[1] > (len(self.map_tiles) - 1):
            return


        unit.update_position(new_position)

    def determine_camera(self):
        max_y_position = len(self.map_tiles) #- config.SCREEN_HEIGHT / config.TILE_HEIGHT
        y_position = self.player.position[1] - math.ceil(round(config.SCREEN_HEIGHT/ config.TILE_HEIGHT / 2))

        if y_position <= max_y_position and y_position >= 0:
            self.camera[1] = y_position
        elif y_position < 0:
            self.camera[1] = 0
        else:
            self.camera[1] = max_y_position

#pygame.init()
#sound = pygame.mixer.Sound('poke_road_sound.wav')
#sound.play()