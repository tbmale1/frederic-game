import pygame
import config


class Player:
    def __init__(self, x_postition, y_position):
        print("player created")
        self.position = [x_postition, y_position]
        # mieux avec chemin relatif
        # self.image = pygame.image.load("F:\ISN\Projet ISN\pygame\Paupau.png")
        self.image = pygame.image.load("perso_milie.png")
        self.image = pygame.transform.scale(self.image, (config.TILE_WIDTH, config.TILE_HEIGHT))
        self.rect = pygame.Rect(self.position[0] * config.TILE_WIDTH, self.position[1] * config.TILE_HEIGHT, config.TILE_WIDTH,
                                config.TILE_HEIGHT)

    def update_position(self, new_position):
        self.position[0] = new_position[0]
        self.position[1] = new_position[1]

    def render(self, screen, camera):
        self.rect = pygame.Rect(self.position[0] * config.TILE_WIDTH,
                                self.position[1] * config.TILE_HEIGHT - (camera[1] * config.TILE_HEIGHT), config.TILE_WIDTH,
                                config.TILE_HEIGHT)
        screen.blit(self.image, self.rect)

